// The coins are 110 cm away from the top wall.

#include <Servo.h>
Servo leftservo;  
Servo rightservo;  
const int pingPin = 5; // Trigger Pin of Ultrasonic Sensor
const int echoPin = 6; // Echo Pin of Ultrasonic Sensor
long coinDistance = 110; // In cm
long difference;
long duration;
long distance;
int correctionTime;
long dp = 13.51; // delta proportional constant (to be calibrated)

void setup() {
  leftservo.attach(9);  
  rightservo.attach(10);
   //set up the Serial
  Serial.begin(9600);
  //setupt the pin modes  
  pinMode(pingPin, OUTPUT);
  pinMode(echoPin, INPUT);
  leftservo.write(90);
  rightservo.write(90);
}

long getDistance() {
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  //send the 10 microsecond trigger
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(pingPin, LOW);
  //get the pulse duration in microseconds
  duration = pulseIn(echoPin, HIGH);
  return distance = (duration * 0.034) / 2;
}

void stop() {
  leftservo.write(90);
  rightservo.write(90);
}

void forward(int time) {
  leftservo.write(180);
  rightservo.write(0);
  delay(time);
}

void rotate90Deg(int direction) {
  // Rotate CCW
  if (direction == 1) {
    leftservo.write(10);
    rightservo.write(10);
    delay(3082);
    stop();
  }
  // Rotate CW
  if (direction == 0) {
    leftservo.write(170);
    rightservo.write(170);
    delay(3082);
    stop();
  }
}

void loop() {
  // Find out distance to be corrected for
  difference = getDistance() - coinDistance;
  Serial.println(difference);
  correctionTime = abs(difference * dp);

  // rotate bot
  if (difference > 5) {
    // rotate twords wall
    rotate90Deg(1);
    // correct distance
    forward(correctionTime);
    rotate90Deg(0);
  } else if (difference < -5) {
    //rotate away from wall
    rotate90Deg(0);
    //correct distance
    forward(correctionTime);
    rotate90Deg(1);
  } else {
    forward(1000);
  }
}
