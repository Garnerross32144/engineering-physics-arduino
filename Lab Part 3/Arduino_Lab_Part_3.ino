/*--------- Move a Robot ---------*/

#include <Servo.h>
Servo leftservo;  
Servo rightservo;  
void setup() {
  leftservo.attach(11);  
  rightservo.attach(10); 
  //move forward fast
  leftservo.write(180);
  rightservo.write(0);

  delay(1000);
  leftservo.write(89);
  rightservo.write(89);
}
void loop() {
}
