// The coins are around 110 cm away from the top wall.

#include <Servo.h>
Servo leftservo;  
Servo rightservo;  
const int pingPin = 5; // Trigger Pin of Ultrasonic Sensor
const int echoPin = 6; // Echo Pin of Ultrasonic Sensor
long coinDistance = 110; // In cm
long distanceToWall;
long duration;
long distance;

void setup() {
  leftservo.attach(9);  
  rightservo.attach(10);
   //set up the Serial
  Serial.begin(9600);
  //setupt the pin modes  
  pinMode(pingPin, OUTPUT);
  pinMode(echoPin, INPUT);
  leftservo.write(90);
  rightservo.write(90);
}

long getDistance() {
  digitalWrite(pingPin, LOW);
  delayMicroseconds(2);
  //send the 10 microsecond trigger
  digitalWrite(pingPin, HIGH);
  delayMicroseconds(10);
  digitalWrite(pingPin, LOW);
  //get the pulse duration in microseconds
  duration = pulseIn(echoPin, HIGH);
  distance = (duration * 0.034) / 2;
  return distance;
}

/*

void loop() {
  // Allign with coins
  if (getDistance() > coinDistance) {
    // Move twords wall
    leftservo.write(45);
    rightservo.write(135);
    while (getDistance() > coinDistance) {
        if (getDistance() < coinDistance) {
          leftservo.write(90);
          rightservo.write(90);
          break;
        }
    }
  } else if (distance < coinDistance) {
    // Move away from wall
        while (distance > coinDistance) {
        leftservo.write(135);
        rightservo.write(45);
  }
  delay(50);  
}

*/
void loop() {
  // Allign with coins
  if (getDistance() > coinDistance) {
    // Move twords wall
    while (getDistance() > coinDistance) {
      leftservo.write(100);
      rightservo.write(65);
    }
    leftservo.write(90);
    rightservo.write(90);
    }
  delay(50);
}